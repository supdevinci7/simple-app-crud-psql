import unittest
from unittest.mock import patch
from project import app

class TestApp(unittest.TestCase):

    def setUp(self):
        self.app = app.create_app().test_client()

    def test_home_page(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 302)

if __name__ == '__main__':
    unittest.main()